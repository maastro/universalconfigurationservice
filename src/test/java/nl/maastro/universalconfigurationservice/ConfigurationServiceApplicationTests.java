package nl.maastro.universalconfigurationservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import nl.maastro.mia.universalconfigurationservice.ConfigurationServiceApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ConfigurationServiceApplication.class)
public class ConfigurationServiceApplicationTests {
		
	@Test
	public void contextLoads() {
	}

}
