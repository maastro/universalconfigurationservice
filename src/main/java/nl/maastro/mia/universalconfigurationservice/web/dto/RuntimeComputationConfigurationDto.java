package nl.maastro.mia.universalconfigurationservice.web.dto;

import java.util.ArrayList;
import java.util.List;

public class RuntimeComputationConfigurationDto {
    
    private String runtimeComputationName;
    private List<ConfigFileDto> configFiles = new ArrayList<>();
    
    public String getRuntimeComputationName() {
        return runtimeComputationName;
    }
    public void setRuntimeComputationName(String runtimeComputationName) {
        this.runtimeComputationName = runtimeComputationName;
    }
    public List<ConfigFileDto> getConfigFiles() {
        return configFiles;
    }
    public void setConfigFiles(List<ConfigFileDto> configFiles) {
        this.configFiles = configFiles;
    }

    public static class ConfigFileDto {
        
        private String commandParameter;
        private String fileNameOriginal;
        private String fileNameSaved;
        
        public String getCommandParameter() {
            return commandParameter;
        }
        public void setCommandParameter(String commandParameter) {
            this.commandParameter = commandParameter;
        }
        public String getFileNameOriginal() {
            return fileNameOriginal;
        }
        public void setFileNameOriginal(String fileNameOriginal) {
            this.fileNameOriginal = fileNameOriginal;
        }
        public String getFileNameSaved() {
            return fileNameSaved;
        }
        public void setFileNameSaved(String fileNameSaved) {
            this.fileNameSaved = fileNameSaved;
        }
    }
}
