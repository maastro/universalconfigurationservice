package nl.maastro.mia.universalconfigurationservice.implementations;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.configurationserviceplugin.interfaces.ContainerConfigurationServiceInterface;
import nl.maastro.mia.configurationserviceplugin.web.dto.ConfigurationDto;

import javax.naming.ConfigurationException;

@RestController
@RequestMapping("/api")
public class ContainerConfigurationControllerImpl implements ContainerConfigurationServiceInterface {
	
	private final static Logger LOGGER = Logger.getLogger(ContainerConfigurationControllerImpl.class);
	
	@Autowired
	ContainerConfigurationServiceImpl containerConfigurationService;
		
	@RequestMapping(value = "/configuration", method = RequestMethod.GET)
	public ConfigurationDto getConfigurationByInputPort (
			@RequestParam Integer inputPort) throws ConfigurationException {
		LOGGER.debug("get configuration for input port: " + inputPort);
		return containerConfigurationService.getConfigurationByInputPort(inputPort);
	}

}
