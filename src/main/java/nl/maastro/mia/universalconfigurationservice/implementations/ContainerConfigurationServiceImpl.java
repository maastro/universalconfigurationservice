package nl.maastro.mia.universalconfigurationservice.implementations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.naming.ConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.mia.configurationserviceplugin.entity.Computation;
import nl.maastro.mia.configurationserviceplugin.entity.Configuration;
import nl.maastro.mia.configurationserviceplugin.interfaces.ContainerConfigurationServiceInterface;
import nl.maastro.mia.configurationserviceplugin.repository.ConfigurationRepository;
import nl.maastro.mia.configurationserviceplugin.service.RequiredModalityService;
import nl.maastro.mia.configurationserviceplugin.service.VolumeOfInterestService;
import nl.maastro.mia.configurationserviceplugin.web.dto.ComputationDto;
import nl.maastro.mia.configurationserviceplugin.web.dto.ConfigurationDto;
import nl.maastro.mia.configurationserviceplugin.web.dto.ModuleConfigurationDto;
import nl.maastro.mia.universalconfigurationservice.web.dto.RuntimeComputationConfigurationDto;

@Service
public class ContainerConfigurationServiceImpl implements ContainerConfigurationServiceInterface {
	
	@Autowired
	ConfigurationRepository configurationRepository;
	
	@Autowired
	RequiredModalityService requiredModalityService;
	
	@Autowired
	private VolumeOfInterestService volumeOfInterestService;

	@Autowired
	private ObjectMapper objectMapper;
	
	public ConfigurationDto getConfigurationByInputPort(Integer inputPort) throws ConfigurationException {
		
		Configuration configuration = configurationRepository.findTop1ByInputPort(inputPort);
		if(configuration==null){
		    throw new IllegalArgumentException("No configuration found for inputport " + inputPort);
		}
		return mapConfiguration(configuration);
	}
	
	private ConfigurationDto mapConfiguration(Configuration entity) throws ConfigurationException {
		ConfigurationDto dto = new ConfigurationDto();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setOutputPort(entity.getOutputPort());
		dto.setModulesConfiguration(getModulesConfigurationListDto(entity.getComputations()));
		dto.setVolumesOfInterest(volumeOfInterestService.getVolumesOfInterestDto(entity.getComputations()));
		return dto;
	}
	
	private List<ModuleConfigurationDto> getModulesConfigurationListDto(List<Computation> computations) throws ConfigurationException {
		List<ModuleConfigurationDto> moduleList = new ArrayList<>();
		String lastModuleName = null;

		for(Computation computation : computations){
			
			if(lastModuleName!=null && lastModuleName.equals(computation.getModuleName())){
				addComputation(moduleList.get(moduleList.size()-1), computation);
			}
			else{
				ModuleConfigurationDto module = createModuleConfiguration(computation);
				moduleList.add(module);
				lastModuleName = module.getModuleName();
			}
		}
		
		return moduleList;
	}
	
	
	private ModuleConfigurationDto createModuleConfiguration(Computation computation) throws ConfigurationException {
		ModuleConfigurationDto moduleConfigurationDto = new ModuleConfigurationDto();
		moduleConfigurationDto.setModuleName(computation.getModuleName());
        
		if (computation.getModuleName().equals("RuntimeComputation")) {
		    String computationConfiguration = computation.getComputationConfiguration();
		    if(computationConfiguration==null) {
				throw new ConfigurationException("No configuration set for computation: " + computation.getComputationIdentifier());
			}
		    
		    RuntimeComputationConfigurationDto configDto;
			try {
				configDto = objectMapper.readValue(computationConfiguration, RuntimeComputationConfigurationDto.class);
				computation.setModuleName(configDto.getRuntimeComputationName());
				moduleConfigurationDto.setModuleName(configDto.getRuntimeComputationName());
			} catch (IOException e) {
                ConfigurationException configurationException = new ConfigurationException(
                        "Error parsing RuntimeComputationConfigurationDto for ComputationIdentifier:"
                        + computation.getComputationIdentifier());
                configurationException.setStackTrace(e.getStackTrace());
                throw configurationException;
			}
		}

		moduleConfigurationDto.setRequiredModalities(requiredModalityService.getRequiredModalities(computation.getModuleName()));
		moduleConfigurationDto = addComputation(moduleConfigurationDto, computation);
		return moduleConfigurationDto;
	}
	
	private ModuleConfigurationDto addComputation(ModuleConfigurationDto moduleConfigurationDto, Computation computation){
		moduleConfigurationDto.getComputations().add(mapComputation(computation));
		return moduleConfigurationDto;
	}
	
	private ComputationDto mapComputation(Computation entity){
		ComputationDto dto = new ComputationDto();
		if(null!=entity.getComputationConfiguration()){
			dto.setConfiguration(entity.getComputationConfiguration());
		}
		else{
			dto.setConfiguration("");
		}
		dto.setIdentifier(entity.getComputationIdentifier());
		dto.setModuleName(entity.getModuleName());
		dto.setVolumeOfInterestNames(Arrays.asList(entity.getVolumeOfInterest().getName()));
		return dto;
	}
}