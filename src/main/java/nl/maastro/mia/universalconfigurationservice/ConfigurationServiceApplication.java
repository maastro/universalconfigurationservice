package nl.maastro.mia.universalconfigurationservice;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ConfigurationServiceApplication {

	private static final Logger LOGGER = Logger.getLogger(ConfigurationServiceApplication.class);
	private static final String name = ConfigurationServiceApplication.class.getPackage().getName();
	private static final String version = ConfigurationServiceApplication.class.getPackage().getImplementationVersion();
		
	public static void main(String[] args) {    	
		SpringApplication.run(ConfigurationServiceApplication.class, args);
		LOGGER.info("**** VERSION **** : " + name + " " +  version );  
	}
}
