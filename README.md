MIA Configuration Service
======================

The configurationservice keeps track of and stores all configuration for the [Medical Image Analysis framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home). The default configuration service keeps track of configured Volume Of Interests, Computations, and computation configurations linked to a DICOM C-STORE SCP.

## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser 
   
## Usage ##

 The [swagger interface](http://localhost:8200/swagger-ui.html) will provide a summary of all possible functions and requests.